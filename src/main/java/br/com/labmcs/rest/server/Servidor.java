package br.com.labmcs.rest.server;

import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class Servidor {
	
	private static HttpServer server;

	public static void startServer() {
		// Especificando pacote que será escaneado tudo do JAX RS para ser usando na API Rest
		ResourceConfig config = new ResourceConfig().packages("br.com.labmcs.rest.resources");
		URI uri = URI.create("http://localhost:8180/");
		server = GrizzlyHttpServerFactory.createHttpServer(uri, config);
	}
	
	public static void stopServer() {
		if(server != null && server.isStarted()) {
			server.shutdown();
		}
	}
	
	
	public static void main(String[] args) {
		Servidor.startServer();	
	}
}

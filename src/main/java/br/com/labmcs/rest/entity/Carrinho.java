package br.com.labmcs.rest.entity;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

public class Carrinho {

	private Long id;
	private String name;
	private String espcificacao;
	private List<Produto> listaDeProdutos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEspcificacao() {
		return espcificacao;
	}

	public void setEspcificacao(String espcificacao) {
		this.espcificacao = espcificacao;
	}

	public String toJson() {
		return new Gson().toJson(this);
	}

	public List<Produto> getListaDeProdutos() {
		return listaDeProdutos;
	}

	public void setListaDeProdutos(List<Produto> listaDeProdutos) {
		this.listaDeProdutos = listaDeProdutos;
	}

	public void adicionaProdudo(Produto produto) {
		if (this.listaDeProdutos == null) {
			this.listaDeProdutos = new ArrayList<>();
		}
		this.listaDeProdutos.add(produto);
	}

}

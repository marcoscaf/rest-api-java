package br.com.labmcs.rest.resources;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import br.com.labmcs.rest.dao.CarrinhoDAO;
import br.com.labmcs.rest.entity.Carrinho;
import br.com.labmcs.rest.entity.Produto;

@Path("carrinhos")
public class CarrinhoResource {


	/**
	 * @GET = Indicando que o método será invocado por um get
	 * @Produces = Indica que o método produz um retorno do tipo especificado (nesse exemplo é um JSON)
	 * @QueryParam("id") = Indica que na URI o cliente terá que passar parametros no seguinte formato: http://localhost:8080/carrinhos?id=1
	 **/
	/*@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String busca(@QueryParam("id") long id) {
		return new CarrinhoDAO().pegaCarrinho(id).toJson();
	}*/
	
	/**
	 * @GET = Indicando que o método será invocado por um get
	 * @Path("{id}") = Indica que o parametro 'id' será passado na própria URI 'http://localhost:8080/carrinhos/1' (Addressability)
	 * @Produces = Indica que o método produz um retorno do tipo especificado (nesse exemplo é um JSON)
	 * @PathParam("id") = Indica que na URI o cliente terá que passar parametros no seguinte formato: http://localhost:8080/carrinhos/1
	 **/
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String busca(@PathParam("id") long id) {
		return new CarrinhoDAO().pegaCarrinho(id).toJson();
	}
	
	
	/**
	 * @POST = Indicando que o método será invocado por um post
	 * @Consumes = Indica que o método consome um retorno do tipo especificado (nesse exemplo é um JSON)
	 **/
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response adiciona(String carrinho) throws URISyntaxException {
		Carrinho carrinhoFromJson = new Gson().fromJson(carrinho, Carrinho.class);
		new CarrinhoDAO().adicionaNovoCarrinho(carrinhoFromJson);
		
		// Retorna para o cliente um Response com o status code 201 = Created
		// https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml
		URI createdURI = new URI("carrinhos/"+carrinhoFromJson.getId());
 		return Response.created(createdURI).build();
	}
	
	
	/**
	 * A URI http://localhost:8180/carrinhos/1/produtos/123, invocará o método abaixo para remover um produto do carrinho.
	 *  
	 **/
	@Path("{id}/produtos/{produtoId}")
	@DELETE
	public Response removeProduto(@PathParam("id") long carrinhoId, @PathParam("produtoId") long produtoId) {
		
		// Lógica de remoção aqui...
		System.out.println("Produto id:"+produtoId+" removido do carrinho id:"+carrinhoId);
		return Response.ok().build();
		
	}
	
	/**
	 * A URI http://localhost:8180/carrinhos/1/produtos/123, invocará o método abaixo para alterar um produto do carrinho.
	 *  
	 **/
	@Path("{id}/produtos/{produtoId}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response aletraProduto(String novoProduto, @PathParam("id") long carrinhoId, @PathParam("produtoId") long antigoProdutoId) {
		Gson gson = new Gson();
		Produto produtoFromJSON = gson.fromJson(novoProduto, Produto.class);
		
		
		// Lógica de remoção aqui...
		System.out.println("Alterando pdoduto XPTO id:"+antigoProdutoId+" pelo produto "+produtoFromJSON.getNomeProduto() +" id:"+produtoFromJSON.getId());
		return Response.ok().build();
		
	}
	
	
	/**
	 * A URI http://localhost:8180/carrinhos/1/produtos/123/200, invocará o método abaixo para alterar a quantidade do produto no carrinho.
	 *  
	 **/
	@Path("{id}/produtos/{produtoId}/{quantidade}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response aletraQuantidadeProduto(@PathParam("id") long carrinhoId, @PathParam("produtoId") long produtoId, @PathParam("quantidade") int quantidade) {
		
		// Lógica de remoção aqui...
		System.out.println("Nova quantidade "+quantidade+" do produto id:"+produtoId);
		return Response.ok().build();
		
	}
	
}

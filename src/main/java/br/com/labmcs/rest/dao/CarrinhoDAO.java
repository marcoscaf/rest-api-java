package br.com.labmcs.rest.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import br.com.labmcs.rest.entity.Carrinho;

public class CarrinhoDAO {
	
	List<Carrinho> listaCarrinho = criaListaCarrinho();

	public Carrinho pegaCarrinho(Long id) {

		Optional<Carrinho> optionalCarrinho = 
				listaCarrinho.stream()
				.filter(carrinho -> id.equals(carrinho.getId()))
				.findAny();
		
		if (optionalCarrinho.isPresent()) {
			return optionalCarrinho.get();
		}

		return null;
	}
	
	public void adicionaNovoCarrinho(Carrinho carrinho) {
		listaCarrinho.add(carrinho);
	}

	private List<Carrinho> criaListaCarrinho() {
		List<Carrinho> listaDeCarrinhos = new ArrayList<Carrinho>();

		Carrinho c1 = new Carrinho();
		c1.setId(1l);
		c1.setName("Carrinho 1");
		c1.setEspcificacao("Carrinho 1 - Tipo: 1 - Marca: 1");

		listaDeCarrinhos.add(c1);

		return listaDeCarrinhos;
	}

}

package br.com.labmcs.test;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;

import br.com.labmcs.rest.entity.Carrinho;
import br.com.labmcs.rest.entity.Produto;
import br.com.labmcs.rest.server.Servidor;

public class TestaApiRest {

	private ClientConfig config;

	@Before
	public void startServer() {

		// Criando uma configuração para que seja logado no client tudo o que aconteceu
		// durante a requisição.
		config = new ClientConfig();
		config.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,
				LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));

		System.out.println("Starting server...");
		Servidor.startServer();
	}

	@After
	public void stopServer() {
		Servidor.stopServer();
		System.out.println("Server stoped");
	}

	//@Test
	public void testaSubidaDoServidor() {

		// Cliente Javax WS para consumir um serviço
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target("http://localhost:8180");
		String conteudo = target.path("/carrinhos/1").request().get(String.class);

		// Convertendo o objeto de volta para Carrinho
		Carrinho carrinhoDoServer = new Gson().fromJson(conteudo, Carrinho.class);

		System.out.println("Resposta do Server:" + conteudo);

		System.out.println(carrinhoDoServer.getName());

	}

	//@Test
	public void testaAdicionaCarrinho() {

		Carrinho c1 = new Carrinho();
		c1.setId(2l);
		c1.setName("Carrinho 2");
		c1.setEspcificacao("Carrinho 2");

		// Cliente WS Rest
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target("http://localhost:8180");

		// Montando a entidade que será enviada para o post do servidor
		// A entidade é composta pelo carrinho no formato json
		// E essa entidade é enviada para o path /carrinhos
		Entity<String> entity = Entity.entity(new Gson().toJson(c1), MediaType.APPLICATION_JSON);
		Response response = target.path("/carrinhos").request().post(entity);

		// Verificando se o status é 201 - Created
		System.out.println(response.getStatus());
		Assert.assertEquals(201, response.getStatus());

	}

	//@Test
	public void testaRemoveProduto() {

		// Cliente WS Rest
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target("http://localhost:8180");

		// Passando o request de DELTE para efetivar a remoção do recurso no server.
		Response response = target.path("/carrinhos/1/produtos/123456").request().delete();

		// Verificando se o status é 200 - OK
		System.out.println(response.getStatus());
		Assert.assertEquals(200, response.getStatus());

	}

	@Test
	public void testaALteraProduto() {

		// Cliente WS Rest
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target("http://localhost:8180");

		Produto novoProduto = new Produto();
		novoProduto.setId(456789l);
		novoProduto.setDescricaoProduto("PS4 PRO");
		novoProduto.setNomeProduto("PS4 PRO");
		
		Entity<String> produtoEntity = Entity.entity(new Gson().toJson(novoProduto), MediaType.APPLICATION_JSON);
		
		// Passando o request de DELTE para efetivar a remoção do recurso no server.
		Response response = target.path("/carrinhos/1/produtos/123456").request().put(produtoEntity);

		// Verificando se o status é 200 - OK
		System.out.println(response.getStatus());
		Assert.assertEquals(200, response.getStatus());

	}
	
	@Test
	public void testaALteraQuantidadeProduto() {

		// Cliente WS Rest
		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target("http://localhost:8180");

		Produto novoProduto = new Produto();
		novoProduto.setId(456789l);
		novoProduto.setDescricaoProduto("PS4 Slim");
		novoProduto.setNomeProduto("PS4 Slim");
		
		Entity<String> produtoEntity = Entity.entity(new Gson().toJson(novoProduto), MediaType.APPLICATION_JSON);
		
		// Passando o request de DELTE para efetivar a remoção do recurso no server.
		Response response = target.path("/carrinhos/1/produtos/123456/200").request().put(produtoEntity);

		// Verificando se o status é 200 - OK
		System.out.println(response.getStatus());
		Assert.assertEquals(200, response.getStatus());

	}

}
